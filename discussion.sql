
-- - lady gaga
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018", 1002);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 181, "Rock and roll", 14);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 14);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011", 1002);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 15);


-- JUSTIN BIEBER
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015", 1003);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 192, "Dancehall-poptropical housemoombahton", 16);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012", 1003);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 17);


-- ARIANNA GRANDE
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016", 1004);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 18);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019", 1004);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 196, "Pop, R&B", 19);


-- Bruno Mars:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016", 1005);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 20);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011", 1005);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 192, "Pop", 21);


-- [SECTION] Advance Selects

-- Exclude records or NOT operator
SELECT * FROM songs WHERE id != 3;

-- GREATER THAN OR EQUAL TO;
SELECT * FROM songs WHERE id > 3;
SELECT * FROM songs WHERE id >= 3;

-- LESS THAN OR EQUAL TO;
SELECT * FROM songs WHERE id < 11;
SELECT * FROM songs WHERE id <= 11;

-- IN operator
SELECT * FROM songs WHERE id IN (1, 3, 11);
SELECT * FROM songs WHERE GENRE IN ("Pop", "KPOP");

-- Partial matches
-- % = wildcard
-- if %a is used, it will return all the records that has a in the end of the word
-- if a% is used, it will return all the records that has a in the start of the word
-- if %a% is used, it will return all the records that has a in the middle of the word
-- if a%a is used, it will return all the records that has a in the start and end of the word

SELECT * FROM songs WHERE song_name LIKE "%a";

-- use the _ to match a single character
SELECT * FROM songs WHERE song_name LIKE "%s_y%";

-- use the % to match multiple characters with the same pattern
SELECT * FROM songs WHERE song_name LIKE "%s%y%";

-- use the ^ to match the start of the string
SELECT * FROM songs WHERE song_name LIKE "^s%y%";

-- use the $ to match the end of the string
SELECT * FROM songs WHERE song_name LIKE "%s%y$";



-- Sorting records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Getting distinct records
SELECT DISTINCT genre FROM songs;

-- Getting the number of records
SELECT COUNT(genre) FROM songs WHERE genre = "Pop";

-- Getting the sum of records
SELECT SUM(length) FROM songs WHERE genre = "Pop";



-- [INNER JOIN] - Joining two tables
-- join two tables together using the common column between them
--  column that has no value will not be returned
SELECT * FROM songs INNER JOIN albums ON songs.album_id = albums.id;
SELECT * FROM albums  INNER JOIN songs ON songs.album_id = albums.id;
SELECT songs.song_name, albums.album_title, albums.artist_id FROM songs INNER JOIN albums ON songs.album_id = albums.id;
SELECT songs.song_name, albums.album_title, albums.artist_id FROM albums  INNER JOIN songs ON songs.album_id = albums.id;

-- [LEFT JOIN] - Joining two tables (left table is the main table)
SELECT * FROM albums LEFT JOIN songs ON songs.album_id = albums.id;
SELECT songs.song_name, albums.album_title, albums.artist_id FROM albums LEFT JOIN songs ON songs.album_id = albums.id;

SELECT * FROM songs LEFT JOIN albums ON songs.album_id = albums.id;


-- Joining Multiple tables
SELECT * FROM artists
    JOIN albums ON albums.artist_id = artists.id
    JOIN songs ON songs.album_id = albums.id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists
    JOIN albums ON albums.artist_id = artists.id
    JOIN songs ON songs.album_id = albums.id;


--  [RIGHT JOIN] - Joining two tables (right table is the main table)
SELECT * FROM albums RIGHT JOIN songs ON songs.album_id = albums.id;


-- [OUTER JOIN] - Joining two tables (both tables are the main table)
SELECT * FROM albums LEFT OUTER JOIN songs ON  albums.id = songs.album_id
    UNION
    SELECT * FROM albums RIGHT OUTER JOIN songs ON albums.id = songs.album_id;


